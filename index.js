const express = require('express');
const app = express();

//route handler
app.get('/', (req, res) => {
    res.send({bye: 'tim'})
});

// request
// get - get info
// post - send info
// app, put - update all the properties of something
// delete - delete 
// patch - update one or two 

//req - request
//res - response

const PORT = process.env.PORT || 5000;
app.listen(PORT);
// || - this is a boolean



